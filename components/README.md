# Beware of fake components!

- We first tried cl_components, but that converted everything to arrays.
- Then we tried to wrap in an array, but wont work.
- Then we tried with sdc module, but wont work.

Now we leave the component in place, but map it to old school theme.
- Via symlink with different file name in templates directory.
- Via library definition and attach_library.
