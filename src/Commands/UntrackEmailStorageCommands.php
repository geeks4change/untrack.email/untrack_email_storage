<?php

namespace Drupal\untrack_email_storage\Commands;

use Consolidation\AnnotatedCommand\CommandError;
use Consolidation\OutputFormatters\StructuredData\PropertyList;
use Drupal\untrack_email_storage\UntrackEmailProcessor;
use Drush\Commands\DrushCommands;
use Geeks4change\UntrackEmailAnalyzer\Api;

class UntrackEmailStorageCommands extends DrushCommands {

  /**
   * Debug UntrackEmail processing.
   *
   * @param $name
   *   The name of the email example. Leave empty to see options.
   *
   * @command ute:debug-process
   * @bootstrap full
   * @throws \Exception
   */
  public function debugProcess(?string $name = NULL): PropertyList {
    $files = Api::getTestEmailFileNames();
    if (!$name || !isset($files[$name])) {
      $names = implode(' ', array_keys($files));
      throw new \Exception("Valid names are: $names");
    }
    $file = $files[$name];
    $emailMessage = file_get_contents($file);
    $processor = \Drupal::service('untrack_email_storage.processor');
    assert($processor instanceof UntrackEmailProcessor);
    $uteFullResult = $processor->process($emailMessage);
    $data = [
      'full' => $uteFullResult->id(),
      'url' => $uteFullResult->getSecretReportUrl()->toString(),
    ];
    return new PropertyList($data);
  }

}
