<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage;

use Drupal\untrack_email_storage\Email\EmailToSenderService;
use Drupal\untrack_email_storage\Entity\Interface\UteFullResultInterface;
use Drupal\untrack_email_storage\Entity\Interface\UtePersistentResultInterface;
use Drupal\untrack_email_storage\Entity\UteFullResult;
use Drupal\untrack_email_storage\Entity\UteList;
use Drupal\untrack_email_storage\Entity\UtePersistentResult;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Analyzer;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Result\FullResultWrapper;

final class UntrackEmailProcessor {

  public function __construct(
    protected Analyzer $analyzer,
    protected DomainExtractor $domainExtractor,
    protected EmailToSenderService $emailToSenderService,
  ) {}


  public function process(string $emailMessage): ?UteFullResult {
    $fullResultWrapper = $this->analyzer->analyze($emailMessage);
    $isDkimValid = $fullResultWrapper->fullResult?->details?->dkimResult?->status?->isValid();
    if ($isDkimValid) {
      $uteFullResult = UteFullResult::createItem($fullResultWrapper);
      $uteFullResult->save();
      return $uteFullResult;
    }
    return NULL;
  }

  public function canCreatePersistentResultEntity(UteFullResultInterface $uteFullResult): bool {
    return $uteFullResult->getMaybeFullResult()
      && $uteFullResult->getMaybeFullResult()
        ->details->dkimResult->status->isValid();
  }

  public function createPersistentResultEntity(UteFullResultInterface $uteFullResult): ?UtePersistentResultInterface {
    $maybeFullResult = $uteFullResult->getMaybeFullResult();
    if (!$maybeFullResult) {
      return NULL;
    }
    $fullResultWrapper = new FullResultWrapper($uteFullResult->getLog(), $maybeFullResult);
    $listInfo = $fullResultWrapper->fullResult->listInfo;
    $domain = $this->domainExtractor->extractRegistrableDomain($listInfo->emailAddress);
    $list = UteList::loadOrCreateItem($listInfo, $domain);
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $utePersistentResult = UtePersistentResult::createItem(
      $fullResultWrapper->getPersistentResult(),
      $list
    );
    $utePersistentResult->save();
    $uteFullResult->setPersistentResultEntity($utePersistentResult);
    $uteFullResult->save();

    $this->emailToSenderService->registerPersistentResult($utePersistentResult);
    return $utePersistentResult;
  }

}
