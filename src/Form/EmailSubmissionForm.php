<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\untrack_email_storage\UntrackEmailProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class EmailSubmissionForm extends FormBase {

  use StringTranslationTrait;

  protected UntrackEmailProcessor $untrackEmailProcessor;

  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->untrackEmailProcessor = $container->get('untrack_email_storage.processor');
    return $instance;
  }

  public function getFormId() {
    return 'site_untrack_email__submission';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['email_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message with headers'),
      '#description' => $this->t('Copy the internal text from your email reader.')
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Su bmit'),
    ];
    $form['#build_info']['cache'] = TRUE;
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $emailMessage = $form_state->getValue('email_message');
    $fullResult = $this->untrackEmailProcessor->process($emailMessage);
    if (!$fullResult) {
      $form_state->setErrorByName('email_message', t('The message is not a valid email source, or contains no DKIM signature.'));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $emailMessage = $form_state->getValue('email_message');
    $fullResult = $this->untrackEmailProcessor->process($emailMessage);
    // Ensured in ::validateForm().
    assert($fullResult);
    $form_state->setRedirectUrl($fullResult->getSecretReportUrl());
  }

}
