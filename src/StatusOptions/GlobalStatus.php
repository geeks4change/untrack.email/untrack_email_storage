<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\StatusOptions;

enum GlobalStatus: string {

  use LabelsTrait;

  case UserTracking = 'user_tracking';
  case Clean = 'clean';
  case Cleaned = 'chastized';

  public function label() {
    return match ($this) {
      self::UserTracking => t('User tracking'),
      self::Clean => t('Clean'),
      self::Cleaned => t('Cleaned'),
    };
  }

}
