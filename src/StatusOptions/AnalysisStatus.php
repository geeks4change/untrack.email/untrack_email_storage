<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\StatusOptions;

use function t;

enum AnalysisStatus: string {

  use LabelsTrait;

  case UserTrackingSure = 'analysis_user_tracking_sure';
  case UserTrackingLikely = 'analysis_user_tracking_likely';
  case UserTrackingUnknown = 'analysis_undetermined';
  case UserTrackingUnlikely = 'analysis_user_tracking_unlikely';

  public function label() {
    return match ($this) {
      self::UserTrackingSure => t('User tracking sure'),
      self::UserTrackingLikely => t('User tracking likely'),
      self::UserTrackingUnknown => t('User tracking unknown'),
      self::UserTrackingUnlikely => t('User tracking unlikely'),
    };
  }

  public function hasUserTracking(): bool {
    return $this === self::UserTrackingSure || $this === self::UserTrackingLikely;
  }

}
