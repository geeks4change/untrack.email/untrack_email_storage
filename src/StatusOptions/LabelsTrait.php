<?php

namespace Drupal\untrack_email_storage\StatusOptions;

trait LabelsTrait {

  public static function labels(): array {
    return iterator_to_array((function () {
      foreach (self::cases() as $case) {
        yield $case->value => $case->label();
      }
    })());
  }

  abstract private function label();

  // @todo Re-add this once PHPStorm knows enum::cases() and does not error in
  //   the consuming class.
  // abstract private static function cases(): Iterable;

}
