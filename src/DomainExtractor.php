<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage;

use Drupal\domainparser\DomainParserProviderInterface;

final class DomainExtractor {

  public function __construct(
    protected DomainParserProviderInterface $domainParserProvider,
  ) {}

  public function extractRegistrableDomain(string $emailAddress): string {
    [, $fullDomain] = explode('@', $emailAddress);
    $parser = $this->domainParserProvider->providePublicSuffixListParser();
    return $parser->resolve($fullDomain)->registrableDomain()->toString();
  }

}
