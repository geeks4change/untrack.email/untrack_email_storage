<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Entity\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;

final class UteEntityPermissions {

  const TECHNICIAN = 'untrack_email_storage: technician';
  const ADMIN = 'untrack_email_storage: admin';

  public static function isAtLeastAdmin(AccountInterface $account): AccessResultInterface {
    return AccessResult::allowedIfHasPermissions($account, [
      UteEntityPermissions::ADMIN,
      UteEntityPermissions::TECHNICIAN,
    ], 'OR');
  }

  public static function isAtLeastTechnician(AccountInterface $account): AccessResultInterface {
    return AccessResult::allowedIfHasPermissions($account, [
      UteEntityPermissions::TECHNICIAN,
    ], 'OR');
  }

}
