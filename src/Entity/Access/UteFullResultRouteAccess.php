<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Entity\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\untrack_email_storage\Entity\Interface\UteFullResultInterface;

/**
 * Route access.
 *
 * All entity admin routes are only for technician, except list edit (admin).
 */
final class UteFullResultRouteAccess {

  public static function fullResultReportAccess(UteFullResultInterface $ute_full_result, string $ute_full_result_token): AccessResultInterface {
    return self::hasTokenAccess($ute_full_result, $ute_full_result_token);
  }

  /**
   * Access provider for entity access, where wo donÄt have RouteMatch.
   */
  public static function routeMatchHasSecretToken(UteFullResultInterface $uteFullResult): AccessResultInterface {
    return self::hasTokenAccess($uteFullResult, \Drupal::routeMatch()->getParameter('ute_full_result_token'));
  }

  protected static function hasTokenAccess(UteFullResultInterface $uteFullResult, ?string $ute_full_result_token): AccessResultInterface {
    return AccessResult::allowedIf($ute_full_result_token === $uteFullResult->getConfirmationSecret())
      ->addCacheableDependency($uteFullResult)
      ->addCacheContexts(['route']);
  }

}
