<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Entity\Access;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\untrack_email_storage\Utility\ClosureTool;
use Symfony\Component\Routing\RouteCollection;

final class UteRouteSubscriber extends RouteSubscriberBase {

  protected function alterRoutes(RouteCollection $collection) {
    // Anon may view our entities but should not see our route.
    foreach ([
               'ute_list',
               'ute_sender',
               'ute_full_result',
             ] as $entityTypeId) {
      $route = $collection->get("entity.{$entityTypeId}.canonical");
      $route->setRequirement('_custom_access',
          ClosureTool::toString(UteEntityPermissions::isAtLeastAdmin(...)));
    }
  }

}
