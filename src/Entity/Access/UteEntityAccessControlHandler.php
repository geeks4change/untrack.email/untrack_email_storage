<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Entity\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\untrack_email_storage\Entity\Interface\UteFullResultInterface;
use Drupal\untrack_email_storage\Entity\Interface\UteListInterface;
use Drupal\untrack_email_storage\Entity\Interface\UtePersistentResultInterface;
use Drupal\untrack_email_storage\Entity\Interface\UteSenderInterface;

final class UteEntityAccessControlHandler extends EntityAccessControlHandler {

  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    // Parent already checks technician access, so no need to do mere.
    return parent::checkCreateAccess($account, $context, $entity_bundle);
  }

  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    // Entity type has no access permission defined, we do it here.
    $access = parent::checkAccess($entity, $operation, $account);

    // https://www.drupal.org/project/drupal/issues/3275828
    // Only technician may delete.
    if ($operation === 'delete') {
      $access = $access->orIf(UteEntityPermissions::isAtLeastTechnician($account));
    }

    // Only admin can edit persistent result, everyone view.
    if ($entity instanceof UtePersistentResultInterface) {
      if ($operation === 'update') {
        $access = UteEntityPermissions::isAtLeastTechnician($account);
      }
      elseif ($operation === 'view') {
        $access = $access->orIf(AccessResult::allowed());
      }
    }

    // Only admin and submitter (with secret) can view full result.
    // Repeat the check from route access to be sure.
    if ($entity instanceof UteFullResultInterface) {
      if ($operation === 'update') {
        $access = UteEntityPermissions::isAtLeastTechnician($account);
      }
      if ($operation === 'view') {
        $isAdmin = UteEntityPermissions::isAtLeastAdmin($account);
        $hasSecret = UteFullResultRouteAccess::routeMatchHasSecretToken($entity);
        $access = $isAdmin->orIf($hasSecret);
      }
    }

    // List: Only admin may update, everyone view.
    // This is used by edit_form route automatically.
    if ($entity instanceof UteListInterface) {
      if ($operation === 'update') {
        $access = $access->orIf(UteEntityPermissions::isAtLeastAdmin($account));
      }
      elseif ($operation === 'view') {
        $access = $access->orIf(AccessResult::allowed());
      }
    }

    // Sender: Only technician may update, everyone view.
    if ($entity instanceof UteSenderInterface) {
      if ($operation === 'update') {
        $access = $access->orIf(UteEntityPermissions::isAtLeastTechnician($account));
      }
      elseif ($operation === 'view') {
        $access = $access->orIf(AccessResult::allowed());
      }
    }
    return $access;
  }

  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    $access = parent::checkFieldAccess($operation, $field_definition, $account, $items);
    // We can't rely on $items to get entity type.
    // For a view check without items see ViewExecutable->_initHandler()
    if ($field_definition->getTargetEntityTypeId() === 'ute_list') {
      // Restrict List edit of non 'audit' fields to technician.
      // https://www.drupal.org/project/drupal/issues/3275828
      if ($operation === 'edit') {
        if (!in_array($field_definition->getName(), UteListInterface::ADMIN_EDITABLE_FIELDS)) {
          $access = $access->andIf(UteEntityPermissions::isAtLeastTechnician($account));
        }
      }
    }
    elseif ($field_definition->getTargetEntityTypeId() === 'ute_full_result') {
      // For full result, the entity is already restricted to admin, or
      // permitted user with secret report link.
      // Restrict log to admin.
      if ($field_definition->getName() === 'log') {
        $access = $access->andIf(UteEntityPermissions::isAtLeastAdmin($account));
      }
      // Restrict confirmation secret to technician.
      if ($field_definition->getName() === 'confirmation_secret') {
        $access = $access->andIf(UteEntityPermissions::isAtLeastTechnician($account));
      }
    }
    /** @noinspection PhpStatementHasEmptyBodyInspection */
    elseif ($field_definition->getTargetEntityTypeId() === 'ute_persistent_result') {
      // For persistent result, the entity is already restricted to admin.
      // So nothing more to do.
    }
    return $access;
  }

}
