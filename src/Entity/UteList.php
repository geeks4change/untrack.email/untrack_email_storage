<?php

namespace Drupal\untrack_email_storage\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\untrack_email_storage\Entity\Interface\UteListInterface;
use Drupal\untrack_email_storage\Entity\Interface\UteSenderInterface;
use Drupal\untrack_email_storage\Entity\Interface\UtePersistentResultInterface;
use Drupal\untrack_email_storage\StatusOptions\GlobalStatus;
use Drupal\untrack_email_storage\Utility\EntityStorageTrait;
use Drupal\untrack_email_storage\Utility\WebformTool;
use Drupal\webform\WebformSubmissionInterface;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Result\ListInfo;

/**
 * Defines the list entity class.
 *
 * With revision UI, @see https://www.drupal.org/node/3160443
 *
 * @ContentEntityType(
 *   id = "ute_list",
 *   label = @Translation("List"),
 *   label_collection = @Translation("Lists"),
 *   label_singular = @Translation("list"),
 *   label_plural = @Translation("lists"),
 *   label_count = @PluralTranslation(
 *     singular = "@count lists",
 *     plural = "@count lists",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\untrack_email_storage\Entity\ListBuilder\UteListListBuilder",
 *     "views_data" = "Drupal\entity\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\untrack_email_storage\Entity\Form\UteListForm",
 *       "edit" = "Drupal\untrack_email_storage\Entity\Form\UteListForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision-delete" = \Drupal\Core\Entity\Form\RevisionDeleteForm::class,
 *       "revision-revert" = \Drupal\Core\Entity\Form\RevisionRevertForm::class,
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "revision" = \Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider::class,
 *     },
 *     "access" = \Drupal\untrack_email_storage\Entity\Access\UteEntityAccessControlHandler::class,
 *   },
 *   base_table = "ute_list",
 *   revision_table = "ute_list_revision",
 *   admin_permission = "untrack_email_storage: technician",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_created" = "revision_timestamp",
 *   },
 *   links = {
 *     "collection" = "/admin/content/ute-list",
 *     "add-form" = "/ute-list/add",
 *     "canonical" = "/ute-list/{ute_list}",
 *     "edit-form" = "/ute-list/{ute_list}/edit",
 *     "delete-form" = "/ute-list/{ute_list}/delete",
 *     "revision" = "/ute-list/{ute_list}/revision/{ute_list_revision}/view",
 *     "revision-delete-form" = "/ute-list/{ute_list}/revision/{ute_list_revision}/delete",
 *     "revision-revert-form" = "/ute-list/{ute_list}/revision/{ute_list_revision}/revert",
 *     "version-history" = "/ute-list/{ute_list}/revisions",
 *   },
 *   field_ui_base_route = "entity.ute_list.admin",
 * )
 */
class UteList extends ContentEntityBase implements UteListInterface {

  use EntityStorageTrait;
  use StringTranslationTrait;

  const WEBFORM_ELEMENT_FOR_LIST = 'list';

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['sender'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Sender'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('target_type', 'ute_sender')
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['email_address'] = BaseFieldDefinition::create('email')
      ->setRevisionable(TRUE)
      ->setLabel(t('Email address (from message)'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);
    $fields['email_label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Email label'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);
    $fields['list_id'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('List ID'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['email_address_override'] = BaseFieldDefinition::create('email')
      ->setRevisionable(TRUE)
      ->setLabel(t('Email address override to send emails to'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);


    $fields['last_analysis'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Last analysis'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('target_type', 'ute_persistent_result')
      // This is not set for a very short time on creation.
      ->setRequired(FALSE)
    ;
    $fields['has_user_tracking'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Has user tracking'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
    ;
    $fields['had_user_tracking'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Had user tracking'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
    ;


    $fields['last_feedback'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Last feedback'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('target_type', 'webform_submission')
      ->setRequired(FALSE)
    ;


    $fields['audit_comment'] = BaseFieldDefinition::create('text_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Audit comment'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'weight' => 50,
        'type' => 'text_textarea',
      ])
    ;
    $fields['audit_date'] = BaseFieldDefinition::create('timestamp')
      ->setRevisionable(TRUE)
      ->setLabel(t('Audit date'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'weight' => 40,
        'type' => 'datetime_timestamp',
      ])
    ;

    $fields['global_status'] = BaseFieldDefinition::create('list_string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Global status'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      // @see ::adjustGlobalStatus
      ->setSetting('allowed_values', GlobalStatus::labels())
      ->setRequired(FALSE)
    ;

    return $fields;
  }

  public static function staticHookEntityPostSave(EntityInterface $entity): void {
    if (
      $entity instanceof UtePersistentResultInterface
      && !$entity->original
    ) {
      $list = $entity->getListEntity();
      $list->setLastAnalysisIfValid($entity);
      if ($entity->getMatchLevel()) {
        $list->save();
      }
    }
    elseif (
      $entity instanceof WebformSubmissionInterface
      && $entity->getWebform()->id() === 'ute_feedback'
      && WebformTool::submissionIsPlaced($entity)
    ) {
      $listId = $entity->getData()[self::WEBFORM_ELEMENT_FOR_LIST] ?? NULL;
      if ($listId && ($list = UteList::load($listId))) {
        $list->setLastFeedback($entity);
        $list->save();
      }
    }
  }

  public function label() {
    return "{$this->getEmailLabel()}<{$this->getEmailAddress()}>";
  }


  public static function loadOrCreateItem(ListInfo $listInfo, string $domain): UteListInterface {
    $ids = self::getEntityStorageByClass(static::class)
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('email_label', $listInfo->emailLabel)
      ->condition('email_address', $listInfo->emailAddress)
      ->condition('list_id', $listInfo->listId ?? NULL)
      ->condition('sender.target_id.entity.domain', $domain)
      ->execute();
    if ($ids) {
      assert(count($ids) === 1);
      $id = reset($ids);
      return static::load($id);
    }
    else {
      return static::create([
        'email_label' => $listInfo->emailLabel,
        'email_address' => $listInfo->emailAddress,
        'list_id' => $listInfo->listId,
        'sender' => UteSender::create([
          'domain' => $domain,
        ]),
      ]);
    }
  }

  public function getEmailAddressToSentTo(): string {
    return $this->getEmailAddressOverride() ?: $this->getEmailAddress();
  }

  protected function getEmailAddressOverride(): string {
    return $this->get('email_address_override')->getString();
  }


  public function getEmailAddress(): string {
    return $this->get('email_address')->getString();
  }

  public function getEmailLabel(): string {
    return $this->get('email_label')->getString();
  }

  public function getListId(): string {
    return $this->get('list_id')->getString();
  }

  public function getSenderEntity(): UteSenderInterface {
    $itemList = $this->get('sender');
    assert($itemList instanceof EntityReferenceFieldItemListInterface);
    return $itemList->referencedEntities()[0];
  }

  protected function setLastAnalysisIfValid(UtePersistentResultInterface $persistentResult): void {
    // Only set last analysis and hasUserTracking if match and no errors.
    if ($persistentResult->getMatchLevel()) {
      // ...and only if newer.
      $newMessageDate = $persistentResult->getMessageDate();
      $currentMessageDate = $this->getLastAnalysis()?->getMessageDate();
      $isNewer = $newMessageDate && $newMessageDate > $currentMessageDate;
      if ($isNewer) {
        $this->setHasUserTracking($persistentResult->hasUserTracking());
        $this->set('last_analysis', $persistentResult);
      }
    }
  }

  protected function getLastAnalysis(): ?UtePersistentResultInterface {
    return $this->get('last_analysis')->entity;
  }

  protected function setGlobalStatus(GlobalStatus $status): void {
    $this->set('global_status', $status->value);
  }

  protected function setHasUserTracking(bool $hadUserTracking): void {
    $this->set('has_user_tracking', $hadUserTracking);
  }

  protected function getHasUserTracking(): bool {
    return $this->get('has_user_tracking')->getValue()[0]['value'] ?? FALSE;
  }

  protected function setHadUserTracking(bool $hadUserTracking): void {
    $this->set('had_user_tracking', $hadUserTracking);
  }

  protected function getHadUserTracking(): bool {
    return $this->get('had_user_tracking')->getValue()[0]['value'] ?? FALSE;
  }

  public function setLastFeedback(WebformSubmissionInterface $webformSubmission): void {
    $this->set('last_feedback', $webformSubmission);
  }

  public function preSave(EntityStorageInterface $storage): void {
    $this->setNewRevision();
    $this->adjustHadUserTracking();
    $this->adjustGlobalStatus();
    parent::preSave($storage);
  }

  protected function adjustHadUserTracking(): void {
    if ($this->getHasUserTracking()) {
      $this->setHadUserTracking(TRUE);
    }
  }


  protected function adjustGlobalStatus(): void {
    $globalStatus = match([
      $this->getHadUserTracking(),
      $this->getHasUserTracking(),
    ]) {
      [FALSE, FALSE] => GlobalStatus::Clean,
      [FALSE, TRUE] => throw new \LogicException(),
      [TRUE, FALSE] => GlobalStatus::Cleaned,
      [TRUE, TRUE] => GlobalStatus::UserTracking,
    };
    $this->setGlobalStatus($globalStatus);
  }

}
