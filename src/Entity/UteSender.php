<?php

namespace Drupal\untrack_email_storage\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\untrack_email_storage\Entity\Interface\UteSenderInterface;
use Drupal\untrack_email_storage\Utility\EntityStorageTrait;

/**
 * Defines the sender entity class.
 *
 * @ContentEntityType(
 *   id = "ute_sender",
 *   label = @Translation("Sender"),
 *   label_collection = @Translation("Senders"),
 *   label_singular = @Translation("sender"),
 *   label_plural = @Translation("senders"),
 *   label_count = @PluralTranslation(
 *     singular = "@count senders",
 *     plural = "@count senders",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\untrack_email_storage\Entity\ListBuilder\UteSenderListBuilder",
 *     "views_data" = "Drupal\entity\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\untrack_email_storage\Entity\Form\UteSenderForm",
 *       "edit" = "Drupal\untrack_email_storage\Entity\Form\UteSenderForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = \Drupal\untrack_email_storage\Entity\Access\UteEntityAccessControlHandler::class,
 *   },
 *   base_table = "ute_sender",
 *   admin_permission = "untrack_email_storage: technician",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/ute-sender",
 *     "add-form" = "/ute-sender/add",
 *     "canonical" = "/ute-sender/{ute_sender}",
 *     "edit-form" = "/ute-sender/{ute_sender}/edit",
 *     "delete-form" = "/ute-sender/{ute_sender}/delete",
 *   },
 *   field_ui_base_route = "entity.ute_sender.admin",
 * )
 */
class UteSender extends ContentEntityBase implements UteSenderInterface {

  use EntityStorageTrait;

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['domain'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Domain'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    return $fields;
  }

  public function label() {
    return $this->getDomain();
  }

  public static function loadOrCreateItem(string $domain) {
    $ids = self::getEntityStorageByClass(static::class)
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('domain', $domain)
      ->execute();
    if ($ids) {
      assert(count($ids) === 1);
      $id = reset($ids);
      return static::load($id);
    }
    else {
      return static::create([
        'domain' => $domain,
      ]);
    }
  }

  public function getDomain(): string {
    return $this->get('domain')->getString();
  }

}
