<?php

namespace Drupal\untrack_email_storage\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the details entity edit forms.
 */
class UteFullResultForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case \SAVED_NEW:
        $this->messenger()->addStatus($this->t('New ful result %label has been created.', $message_arguments));
        $this->logger('untrack_email_storage')->notice('Created new details %label', $logger_arguments);
        break;

      case \SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The full result %label has been updated.', $message_arguments));
        $this->logger('untrack_email_storage')->notice('Updated full result %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.ute_full_result.canonical', ['ute_full_result' => $entity->id()]);

    return $result;
  }

}
