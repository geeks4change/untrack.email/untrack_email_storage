<?php

namespace Drupal\untrack_email_storage\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Url;
use Drupal\untrack_email_storage\Entity\Interface\UteFullResultInterface;
use Drupal\untrack_email_storage\Entity\Interface\UtePersistentResultInterface;
use Drupal\untrack_email_storage\Utility\TimeStampFormatter;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Log\AnalyzerLog;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Result\FullResult;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Result\FullResultWrapper;

/**
 * Defines the details entity class.
 *
 * @ContentEntityType(
 *   id = "ute_full_result",
 *   label = @Translation("Full result"),
 *   label_collection = @Translation("Full results"),
 *   label_singular = @Translation("full result"),
 *   label_plural = @Translation("full results"),
 *   label_count = @PluralTranslation(
 *     singular = "@count full result",
 *     plural = "@count full results",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\untrack_email_storage\Entity\ListBuilder\UteFullResultListBuilder",
 *     "views_data" = "Drupal\entity\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\untrack_email_storage\Entity\Form\UteFullResultForm",
 *       "edit" = "Drupal\untrack_email_storage\Entity\Form\UteFullResultForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = \Drupal\untrack_email_storage\Entity\Access\UteEntityAccessControlHandler::class,
 *   },
 *   base_table = "ute_full_result",
 *   admin_permission = "untrack_email_storage: technician",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/ute-full-result",
 *     "add-form" = "/ute-full-result/add",
 *     "canonical" = "/ute-full-result/{ute_full_result}",
 *     "edit-form" = "/ute-full-result/{ute_full_result}/edit",
 *     "delete-form" = "/ute-full-result/{ute_full_result}/delete",
 *   },
 *   field_ui_base_route = "entity.ute_full_result.admin",
 * )
 */
class UteFullResult extends ContentEntityBase implements UteFullResultInterface {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setDisplayConfigurable('view', TRUE)
      ->setLabel(t('Created'));
    $fields['persistent_result'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Persistent result'))
      ->setSetting('target_type', 'ute_persistent_result')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setReadOnly(TRUE);
    $fields['result'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Result'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    $fields['log'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Log'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    $fields['confirmation_secret'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Confirmation secret'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValueCallback('\Drupal\untrack_email_storage\Entity\UteFullResult::createSecret')
      ->setReadOnly(TRUE);
    return $fields;
  }

  public static function createSecret(): string {
    /** @noinspection PhpUnhandledExceptionInspection */
    return bin2hex(random_bytes(16));
  }

  public static function createItem(FullResultWrapper $fullResultWrapper): static {
    return static::create([
      'result' => [
        'data' => $fullResultWrapper->fullResult,
      ],
      'log' => [
        'data' => $fullResultWrapper->log,
      ],
      'persistent_result' => [
        'entity' => NULL,
      ],
    ]);
  }

  public function label() {
    $timeStamp = $this->getMaybeFullResult()?->messageInfo?->timeStamp;
    return TimeStampFormatter::toIso($timeStamp)
      ?? sprintf('(%s)', TimeStampFormatter::toIso($this->getCreatedDate()))
      ?? parent::label();
  }

  protected function getCreatedDate(): int {
    return $this->get('created')->value;
  }

  public function getConfirmationSecret(): string {
    return $this->get('confirmation_secret')->getString();
  }

  public function getMaybeFullResult(): ?FullResult {
    return $this->get('result')->getValue()[0]['data'] ?? NULL;
  }

  public function getLog(): AnalyzerLog {
    return $this->get('log')->getValue()[0]['data'];
  }

  public function getSecretReportUrl(): Url {
    $url = Url::fromRoute('entity.ute_full_result.report', [
      'ute_full_result' => $this->id(),
      'ute_full_result_token' => $this->getConfirmationSecret()
    ]);
    $url->setOption('absolute', TRUE);
    return $url;
  }

  public function getPersistentResultEntity(): ?UtePersistentResultInterface {
    $itemList = $this->get('persistent_result');
    assert($itemList instanceof EntityReferenceFieldItemListInterface);
    return $itemList->referencedEntities()[0] ?? NULL;
  }

  public function setPersistentResultEntity(UtePersistentResultInterface $utePersistentResult): void {
    $this->get('persistent_result')->setValue(['entity' => $utePersistentResult]);
  }

  public static function staticHookEntityExtraFieldInfo(): array {
    $extra['display']['ute_full_result_secret_report_link'] = [
      'label' => t('Secret report link'),
      'weight' => 100,
      'visible' => TRUE,
    ];
    return ['ute_full_result' => ['ute_full_result' => $extra]];
  }

  public static function staticHookEntityView(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, string $view_mode) {
    if ($entity instanceof UteFullResultInterface) {
      if ($display->getComponent('ute_full_result_secret_report_link')) {
        $build['ute_full_result_secret_report_link'] = [
          '#type' => 'link',
          '#title' => t('Secret report link'),
          '#url' => $entity->getSecretReportUrl(),
        ];
      }
    }
  }

}
