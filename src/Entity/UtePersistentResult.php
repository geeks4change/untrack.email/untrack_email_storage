<?php

namespace Drupal\untrack_email_storage\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\untrack_email_storage\Entity\Access\UteEntityPermissions;
use Drupal\untrack_email_storage\Entity\Interface\UteFullResultInterface;
use Drupal\untrack_email_storage\Entity\Interface\UteListInterface;
use Drupal\untrack_email_storage\Entity\Interface\UtePersistentResultInterface;
use Drupal\untrack_email_storage\Utility\TimeStampFormatter;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Log\AnalyzerLog;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Result\PersistentResult;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Result\PersistentResultWrapper;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Verdict\ResultVerdictMatchLevel;

/**
 * Defines the summary entity class.
 *
 * @ContentEntityType(
 *   id = "ute_persistent_result",
 *   label = @Translation("Persistent result"),
 *   label_collection = @Translation("Persistent results"),
 *   label_singular = @Translation("persistent result"),
 *   label_plural = @Translation("persistent results"),
 *   label_count = @PluralTranslation(
 *     singular = "@count persistent result",
 *     plural = "@count persistent results",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\untrack_email_storage\Entity\ListBuilder\UtePersistentResultListBuilder",
 *     "views_data" = "Drupal\entity\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\untrack_email_storage\Entity\Form\UtePersistentResultForm",
 *       "edit" = "Drupal\untrack_email_storage\Entity\Form\UtePersistentResultForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = \Drupal\untrack_email_storage\Entity\Access\UteEntityAccessControlHandler::class,
 *   },
 *   base_table = "ute_persistent_result",
 *   admin_permission = "untrack_email_storage: technician",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/ute-persistent-result",
 *     "add-form" = "/ute-persistent-result/add",
 *     "canonical" = "/ute-persistent-result/{ute_persistent_result}",
 *     "edit-form" = "/ute-persistent-result/{ute_persistent_result}/edit",
 *     "delete-form" = "/ute-persistent-result/{ute_persistent_result}/delete",
 *   },
 *   field_ui_base_route = "entity.ute_persistent_result.admin",
 * )
 */
class UtePersistentResult extends ContentEntityBase implements UtePersistentResultInterface {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setDisplayConfigurable('view', TRUE)
      ->setLabel(t('Created'));
    $fields['message_date'] = BaseFieldDefinition::create('timestamp')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setLabel(t('Message date'))
      ->setReadOnly(TRUE);
    $fields['list'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('List'))
      ->setSetting('target_type', 'ute_list')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    $fields['result'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Result'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    $fields['log'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Log'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);
    return $fields;
  }

  public static function createItem(PersistentResultWrapper $persistentResultWrapper, UteListInterface $list): static {
    return static::create([
      'result' => [
        'data' => $persistentResultWrapper->persistentResult,
      ],
      'message_date' => $persistentResultWrapper->persistentResult?->messageInfo?->timeStamp,
      'log' => [
        'data' => $persistentResultWrapper->log,
      ],
      'list' => [
        'entity' => $list,
      ],
    ]);
  }

  public function label() {
    $timeStamp = $this->getMaybePersistentResult()?->messageInfo?->timeStamp;
    return TimeStampFormatter::toIso($timeStamp)
      ?? sprintf('(%s)', TimeStampFormatter::toIso($this->getCreatedDate()))
      ?? parent::label();
  }

  public function getMatchLevel(): ResultVerdictMatchLevel {
    return $this->getMaybePersistentResult()?->verdict?->matchLevel;
  }

  public function hasUserTracking(): bool {
    return match ($this->getMatchLevel()) {
      NULL,
      ResultVerdictMatchLevel::Unlikely,
      ResultVerdictMatchLevel::Unknown => FALSE,
      ResultVerdictMatchLevel::Likely,
      ResultVerdictMatchLevel::Sure   => TRUE,
    };
  }

  protected function getCreatedDate(): int {
    return $this->get('created')->value;
  }

  public function getMessageDate(): ?int {
    return $this->get('message_date')->value;
  }

  public function getListEntity(): UteListInterface {
    $itemList = $this->get('list');
    assert($itemList instanceof EntityReferenceFieldItemListInterface);
    return $itemList->referencedEntities()[0];
  }

  public function getMaybePersistentResult(): ?PersistentResult {
    return $this->get('result')->getValue()[0]['data'] ?? NULL;
  }

  public function getSanitizedLog(): AnalyzerLog {
    return $this->get('log')->getValue()[0]['data'];
  }

  public function getMaybeFullResultEntity(bool $accessCheck): ?UteFullResultInterface {
    $storage = \Drupal::entityTypeManager()->getStorage('ute_full_result');
    $ids = $storage->getQuery()
      ->accessCheck($accessCheck)
      ->condition('persistent_result', $this->id())
      ->execute();
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return $ids ? $storage->load(reset($ids)) : NULL;
  }

  public static function staticHookEntityExtraFieldInfo(): array {
    $extra['display']['ute_full_result_secret_report_link'] = [
      'label' => t('Secret report link'),
      'weight' => 100,
      'visible' => TRUE,
    ];
    return ['ute_persistent_result' => ['ute_persistent_result' => $extra]];
  }

  public static function staticHookEntityView(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, string $view_mode) {
    if ($entity instanceof UtePersistentResultInterface) {
      if ($display->getComponent('ute_full_result_secret_report_link')) {
        if (UteEntityPermissions::isAtLeastAdmin(\Drupal::currentUser())) {
          if ($uteFullResult = $entity->getMaybeFullResultEntity(accessCheck: TRUE)) {
            $build['ute_full_result_secret_report_link'] = [
              '#type' => 'link',
              '#title' => t('Report'),
              '#url' => $uteFullResult->getSecretReportUrl(),
            ];
          }
        }
      }
    }
  }

}
