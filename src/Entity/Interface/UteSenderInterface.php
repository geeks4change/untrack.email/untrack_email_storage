<?php

namespace Drupal\untrack_email_storage\Entity\Interface;

use Drupal\Core\Entity\ContentEntityInterface;

interface UteSenderInterface extends ContentEntityInterface {

  public function getDomain(): string;

}
