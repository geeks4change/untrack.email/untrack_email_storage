<?php

namespace Drupal\untrack_email_storage\Entity\Interface;

use Drupal\Core\Entity\ContentEntityInterface;

interface UteListInterface extends ContentEntityInterface {

  const ADMIN_EDITABLE_FIELDS = ['audit_date', 'audit_comment', 'email_address_override'];

  public function getEmailAddress(): string;

  public function getEmailLabel(): string;

  public function getListId(): string;

  public function getSenderEntity(): UteSenderInterface;

}
