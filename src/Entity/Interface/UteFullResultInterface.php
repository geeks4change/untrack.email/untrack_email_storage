<?php

namespace Drupal\untrack_email_storage\Entity\Interface;


use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Url;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Log\AnalyzerLog;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Result\FullResult;

interface UteFullResultInterface extends ContentEntityInterface {

  public function getPersistentResultEntity(): ?UtePersistentResultInterface;

  public function setPersistentResultEntity(UtePersistentResultInterface $utePersistentResult): void;

  public function getConfirmationSecret(): string;

  public function getSecretReportUrl(): Url;

  public function getMaybeFullResult(): ?FullResult;

  public function getLog(): AnalyzerLog;

}
