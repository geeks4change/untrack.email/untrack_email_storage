<?php

namespace Drupal\untrack_email_storage\Entity\Interface;


use Drupal\Core\Entity\ContentEntityInterface;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Log\AnalyzerLog;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Result\PersistentResult;
use Geeks4change\UntrackEmailAnalyzer\Analyzer\Verdict\ResultVerdictMatchLevel;

interface UtePersistentResultInterface extends ContentEntityInterface {

  public function getListEntity(): UteListInterface;

  public function getMaybePersistentResult(): ?PersistentResult;

  public function getMessageDate(): ?int;

  public function getSanitizedLog(): AnalyzerLog;

  public function getMaybeFullResultEntity(bool $accessCheck): ?UteFullResultInterface;

  public function getMatchLevel(): ResultVerdictMatchLevel;

  public function hasUserTracking(): bool;

}
