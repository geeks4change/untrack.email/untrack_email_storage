<?php

declare(strict_types=1);
namespace Drupal\untrack_email_storage\Controller;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\untrack_email_storage\Entity\Interface\UteFullResultInterface;
use Drupal\untrack_email_storage\UntrackEmailProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class UteFullResultConfirmationForm extends FormBase {

  use StringTranslationTrait;

  protected UntrackEmailProcessor $untrackEmailProcessor;

  protected EntityTypeManagerInterface $entityTypeManager;

  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->untrackEmailProcessor = $container->get('untrack_email_storage.processor');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  public function getFormId() {
    return 'ute_full_result_confirm_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, UteFullResultInterface $ute_full_result = NULL, string $ute_full_result_token = NULL) {
    $isValid = $this->untrackEmailProcessor->canCreatePersistentResultEntity($ute_full_result);
    if (!$isValid) {
      $form['ute_full_result'] = [
        '#markup' => $this->t('The email message is invalid and could not be analyzed.'),
      ];
    }
    else {
      $alreadyConfirmed = !!$ute_full_result->getPersistentResultEntity();
      if (!$alreadyConfirmed) {
        $form['ute_full_result'] = [
          '#type' => 'value',
          '#value' => $ute_full_result,
        ];
        $form['token'] = [
          '#type' => 'value',
          '#value' => $ute_full_result_token,
        ];

        $form['confirmation_text'] = [
          '#markup' => $this->t('XXX - Untrack.Entity full result confirmation - XXX'),
        ];
        $form['actions']['#type'] = 'actions';
        $form['actions']['yes'] = [
          '#type' => 'submit',
          '#value' => $this->t('Approve'),
          '#button_type' => 'primary',
          '#name' => 'yes',
        ];
        $form['actions']['no'] = [
          '#type' => 'submit',
          '#value' => $this->t('Decline'),
          '#name' => 'no',
        ];
      }
      else {
        $form['report'] = $this->entityTypeManager
          ->getViewBuilder($ute_full_result->getEntityTypeId())
          ->view($ute_full_result, 'report');

      }
    }
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uteFullResult = $form_state->getValue('ute_full_result');
    assert($uteFullResult instanceof UteFullResultInterface);

    $triggeringElementName = $form_state->getTriggeringElement()['#name'];
    if ($triggeringElementName === 'no') {
      $uteFullResult->delete();
      $form_state->setRedirectUrl(Url::fromUserInput('/'));
    }
    elseif ($triggeringElementName === 'yes') {
      $utePersistentResult = $this->untrackEmailProcessor->createPersistentResultEntity($uteFullResult);
      // We ruled that out via $isValid in form build.
      assert(isset($utePersistentResult));
      // No need to redirect, the controller will show the report view mode.
    }
  }

}
