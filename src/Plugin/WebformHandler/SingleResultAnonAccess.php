<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Plugin\WebformHandler;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * @WebformHandler(
 *   id = "ute_single_result_anon_access",
 *   label = @Translation("Single result anon access"),
 *   category = @Translation("Geeks4Change"),
 *   description = @Translation("Allow anon to access any single result, but not the results list."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 *
 * @see adr-0001-webform-submission-access.md
 */
final class SingleResultAnonAccess extends WebformHandlerBase {

  public function access(WebformSubmissionInterface $webform_submission, $operation, AccountInterface $account = NULL) {
    return AccessResult::allowed();
  }

}
