<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Plugin\EmailBuilder;

use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailBuilderBase;
use Drupal\untrack_email_storage\Entity\Interface\UteListInterface;
use Drupal\webform_preset\Entity\WebformPresetInterface;

/**
 * Email Builder of UntrackEmail going to newsletter sender on analysis confirmation.
 *
 * @EmailBuilder(
 *   id = "untrack_email_storage",
 *   sub_types = { "to_sender_on_confirmation" = @Translation("Email to newsletter sender on analysis confirmation") },
 *   common_adjusters = {"email_subject", "email_body"},
 * )
 */
final class UntrackEmailToNewsletterSenderOnConfirmation extends EmailBuilderBase {

  public function createParams(EmailInterface $email, UteListInterface $uteList = NULL, WebformPresetInterface $webformPreset = NULL) {
    assert(isset($uteList));
    $email->setParam('ute_list', $uteList);
    $email->setParam('webform_preset', $webformPreset);
    $email->setVariable('feedback_url', $webformPreset->getSecretUrl());
  }

  public function build(EmailInterface $email) {
    // Must run in this phase.
    $uteList = $email->getParam('ute_list');
    assert($uteList instanceof UteListInterface);
    // @todo Handle noreply addresses.
    // @todo Consider using info or abuse mail addresses.
    $email->setTo($uteList->getEmailAddressToSentTo());
  }

}
