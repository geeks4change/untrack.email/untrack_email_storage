<?php

namespace Drupal\untrack_email_storage\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\untrack_email_storage\Entity\Access\UteEntityPermissions;

/**
 * Plugin implementation of the 'UtE' formatter.
 *
 * @FieldFormatter(
 *   id = "ute_log",
 *   label = @Translation("UtE Log"),
 *   field_types = {
 *     "map"
 *   }
 * )
 */
class UteLog extends FormatterBase {


  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $i => $item) {
      assert($item instanceof FieldItemInterface);
      $log = $item->getValue()['data'] ?? NULL;
      $element[$i] = [
        '#type' => 'sdc',
        '#component' => 'untrack_email_storage:ute-log',
        '#context' => [
          'log' => $log,
          'isUteAdmin' => UteEntityPermissions::isAtLeastAdmin(\Drupal::currentUser())
            ->isAllowed(),
        ],
      ];
    }
    return $element;
  }

  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getName() === 'log'
      && (
        $field_definition->getTargetEntityTypeId() === 'ute_full_result'
        || $field_definition->getTargetEntityTypeId() === 'ute_persistent_result'
      );
  }

}
