<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Email;

use Drupal\symfony_mailer\EmailFactoryInterface;
use Drupal\untrack_email_storage\Entity\Interface\UtePersistentResultInterface;
use Drupal\untrack_email_storage\Entity\UteList;
use Drupal\webform\Entity\Webform;
use Drupal\webform_preset\Entity\WebformPreset;

final class EmailToSenderService {

  public function __construct(
    protected EmailFactoryInterface $emailFactory,
  ) {}

  public function registerPersistentResult(UtePersistentResultInterface $utePersistentResult) {
    if ($utePersistentResult->hasUserTracking()) {
      $uteList = $utePersistentResult->getListEntity();
      $webform = Webform::load('ute_feedback');
      $expire = \Drupal::time()->getRequestTime() + 5 * 7 * 86400;
      $data = [UteList::WEBFORM_ELEMENT_FOR_LIST => $uteList->id()];
      $webformPreset = WebformPreset::createItem($webform, $data, $expire);
      $this->emailFactory->sendTypedEmail('untrack_email_storage',
        'to_sender_on_confirmation', $uteList, $webformPreset);
    }
  }

}
