<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Utility;

use Drupal\Core\Entity\ContentEntityInterface;

final class ContentEntityViewer {

  public static function view(ContentEntityInterface $entity): array {
    $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId());
    return $viewBuilder->view($entity);
  }

}
