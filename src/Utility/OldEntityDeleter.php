<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Utility;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

final class OldEntityDeleter {

  public function __construct(
    protected string $entityTypeId,
    protected int $maxAge,
    protected string $fieldName,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected TimeInterface $time,
  ) {}

  public static function create(
    string $entityTypId,
    int $maxAge,
    string $fieldName = 'created',
  ) {
    return new static(
      $entityTypId,
      $maxAge,
      $fieldName,
      \Drupal::entityTypeManager(),
      \Drupal::time(),
    );
  }

  public function deleteOldEntities(): int {
    $storage = $this->entityTypeManager
      ->getStorage($this->entityTypeId);
    $limit = $this->time->getRequestTime() - $this->maxAge;
    $ids = $storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition($this->fieldName, $limit, '<')
      ->execute();
    foreach (array_chunk($ids, 50) as $chunkIds) {
      $storage->delete($storage->loadMultiple($chunkIds));
    }
    return count($ids);
  }

}
