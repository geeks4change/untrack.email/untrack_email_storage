<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Utility;

use Drupal\webform\WebformSubmissionInterface;

final class WebformTool {

  public static function submissionIsPlaced(WebformSubmissionInterface $webformSubmission): bool {
    return $webformSubmission->getState() === WebformSubmissionInterface::STATE_COMPLETED
      || $webformSubmission->getState() === WebformSubmissionInterface::STATE_UPDATED;
  }
  
}
