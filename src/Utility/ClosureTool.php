<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Utility;

final class ClosureTool {

  public static function toString(\Closure $closure): string {
    $reflector = new \ReflectionFunction($closure);
    $string = $reflector->getName();
    if ($classReflector = $reflector->getClosureScopeClass()) {
      if (!$reflector->isStatic()) {
        throw new \LogicException('Must be ststic.');
      }
      $string = $reflector->getNamespaceName() . '\\' . $classReflector->getName() . '::' . $string;
    }
    else {
      $string = '\\' . $string;
    }
    return $string;
  }

}
