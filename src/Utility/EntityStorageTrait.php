<?php

namespace Drupal\untrack_email_storage\Utility;

trait EntityStorageTrait {

  protected static function getEntityStorageByClass(string $class) {
    $entity_type_repository = \Drupal::service('entity_type.repository');
    $entity_type_manager = \Drupal::entityTypeManager();
    $entityTypeId = $entity_type_repository->getEntityTypeFromClass($class);
    return $entity_type_manager->getStorage($entityTypeId);
  }

}