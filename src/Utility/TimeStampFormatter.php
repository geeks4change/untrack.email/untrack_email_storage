<?php

declare(strict_types=1);

namespace Drupal\untrack_email_storage\Utility;

final class TimeStampFormatter {

  public static function toIso(?int $timeStamp): ?string {
    return !$timeStamp ? NULL :
      (new \DateTime())->setTimestamp($timeStamp)->format('Y-m-d\TH:i:s');
  }

}
