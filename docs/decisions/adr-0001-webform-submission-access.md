# Webform submission access

## Problem
- We want all ute_feedback webform_submissions to be visible for anon.
- When we give anon the permission 'view all webform results', this works,
  - **but** anon also sees the all results tab (e.g. when filling the form), which we do not want.

## Iteration 
- Anon does NOT have the permission 'view all webform results'
- The ute_feedback webform has our custom SingleResultAnonAccess webform handler
  - **Problem**: The list view is not visible anymore
  - Debugging shows that it's query access because of the joined webform_submission table (which we need to generate the link cleanly).

## Solution
- Anon does NOT have the permission 'view all webform results'
- The ute_feedback webform has our custom SingleResultAnonAccess webform handler
- The hook `untrack_email_storage_views_query_alter` removes the 'webform_submission_access' query tag
