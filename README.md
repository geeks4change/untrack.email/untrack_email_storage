# Unspy.Email: Storage

Part of the unspy.email suite: Analyzes emails for user tracking links.
(The "untrack_email" prefix is for legacy reasons.)

Contains Drupal entities and logic to process emails.

## Installation

Follow the usual module installation process:
https://www.drupal.org/docs/extending-drupal/installing-modules

## What it does

The storage component is responsible for storage of the unspy.email analyzer results (see that), and the complete web frontend:
- Upload of email source file into a form
- Processing via analyzer
- Getting consent to process
- Storing Full Analysis Result (containing all URLs)
- Converting and storing Persistent Analysis Result (containing only anonymized URLs)
- Deleting Full Analysis Results after 90 days
- Referincing Newsletter Lists and Responsible Sender (identified by registrable domain)
- Sending informative Emails to Responsible Sender, containing a secret feedback link
- Accepting feedback from Responsible Sender via webform
- Displaying a table of Responsible Senders, Newsletter Lists, including
  - its "does it spy?" status
  - feedback from Sender

## Data structures
The module defines these entities:
- Full Result: The raw analyzer result
- Persistent Result: The anonymized analyzer result
- List: The newsletter list
- Sender: The sender of the newsletter

## Development

This project is not considered stable and makes no BC promises. It is tightly coupled to the analyzer component, and any API changes there will be reflected here.
